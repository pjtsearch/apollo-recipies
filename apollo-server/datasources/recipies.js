const { DataSource } = require('apollo-datasource');

class RecipeAPI extends DataSource {
  constructor() {
    super();
    this.data = [
      {
        id:0,
        name:"test0",
        description:"a test0",
        ingredients:[
          {
            amount:0,
            unit:"lb",
            name:"test0ingredient"
          }
        ],
        steps:[
          {
            text:"test0step"
          }
        ]
      },
      {
        id:1,
        name:"test1",
        description:"a test 1",
        ingredients:[
          {
            amount:1,
            unit:"lb",
            name:"test1ingredient"
          }
        ],
        steps:[
          {
            text:"test1step"
          }
        ]
      }
    ]
  }
  async queryAllRecipies(){
    return this.data;
  }
  async queryRecipieById({id}){
    let res = this.data.find(recipe=>recipe.id === Number(id))
    return res;
  }
  async queryRecipiesByName({name}){
    let res = this.data.filter(recipe=>recipe.name.includes(name) || name.includes(recipe.name))
    console.log(res);
    return res;
  }
  async mutationAddRecipe({recipeInput}){
    let findId = this.data.find(recipe=>recipe.id === Number(id))
    if (!findId){
      this.data.push(recipeInput);
      return {
        success:true,
        recipe:recipeInput
      }
    }else{
      console.log(findId);
      return {
        success:false,
        recipe:recipeInput
      }
    }
  }
  async mutationEditRecipe({id,recipeInput}){
    let foundRecipe = this.data.find(recipe=>recipe.id === Number(id))
    let oldRecipe = {...foundRecipe};
    foundRecipe = recipeInput;
    return {
      success:true,
      newRecipe:recipeInput,
      oldRecipe:oldRecipe
    }
  }
}

module.exports = RecipeAPI;
