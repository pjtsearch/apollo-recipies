const { ApolloServer } = require('apollo-server');
const typeDefs = require('./schema');

const RecipeAPI = require("./datasources/recipies");

const resolvers = require('./resolvers');

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: () => ({
    RecipeAPI: new RecipeAPI()
  })
});

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});
