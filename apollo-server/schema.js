const { gql } = require('apollo-server');

const typeDefs = gql`
type Query {
  allRecipies:[Recipe]!
  recipe(id:ID!):Recipe
  recipies(name:String!):[Recipe]
}

type Recipe {
  id:ID!
  name:String!
  description:String!
  ingredients:[Ingredient]!
  steps:[Step]!
}

type Step {
  text:String
}

type Ingredient {
  amount:Int
  unit:Unit
  name:String
}

enum Unit {
  lb
  oz
  tsp
  tbsp
  cup
}

type Mutation {
  addRecipe(recipe:RecipeInput!):addRecipeResponse
  editRecipe(id:ID!,recipe:RecipeInput!):editRecipeResponse
}

input RecipeInput {
  id:ID!
  name:String!
  description:String!
  ingredients:[IngredientInput]!
  steps:[StepInput]!
}

input StepInput {
  text:String
}

input IngredientInput {
  amount:Int
  unit:Unit
  name:String
}

type addRecipeResponse {
  success:Boolean
  recipe:Recipe
}

type editRecipeResponse {
  success:Boolean
  newRecipe:Recipe
  oldRecipe:Recipe
}
`

module.exports = typeDefs;
