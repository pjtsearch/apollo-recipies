module.exports = {
  Query: {
    allRecipies: async (_, __, { dataSources }) =>
      dataSources.RecipeAPI.queryAllRecipies(),
    recipe: (_, { id }, { dataSources }) =>
      dataSources.RecipeAPI.queryRecipieById({ id: id }),
    recipies: async (_, {name}, { dataSources }) =>
      dataSources.RecipeAPI.queryRecipiesByName({ name: name }),
  },
  Mutation: {
    async addRecipe(_,{recipe},{ dataSources }){
      return dataSources.RecipeAPI.mutationAddRecipe({recipeInput:recipe})
    },
    async editRecipe(_,{id,recipe},{ dataSources }){
      return dataSources.RecipeAPI.mutationEditRecipe({id:id,recipeInput:recipe})
    }
  }
};
